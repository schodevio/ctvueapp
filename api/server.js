// server.js

// BASE SETUP
// =============================================================================

// call the packages we need
var express    = require('express');        // call express
var api        = express();                 // define our api using express
var bodyParser = require('body-parser');

// configure api to use bodyParser()
// this will let us get the data from a POST
api.use(bodyParser.urlencoded({ extended: true }));
api.use(bodyParser.json());

var port = process.env.PORT || 8080;        // set our port


api.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});


// Networks
var networksRouter = require('./controllers/networks.js');
api.use('/api/networks', networksRouter);

// Plugs
var plugsRouter = require('./controllers/plugs.js');
api.use('/api/plugs', plugsRouter);

// START THE SERVER
// =============================================================================
api.listen(port);
console.log('Magic hapiens on port ' + port);
