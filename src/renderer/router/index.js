import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'homepage-dashboard',
      component: require('@/components/homepage/HomepageDashboard').default
    },
    {
      path: '/plugs',
      name: 'plugs-dashboard',
      component: require('@/components/plugs/PlugsDashboard').default
    },
    {
      path: '/settings',
      // name: 'settings-dashboard',
      component: require('@/components/settings/SettingsDashboard').default,
      children: [
        {
          path: '',
          name: 'general-tab',
          component: require('@/components/settings/general/GeneralTab').default
        },
        {
          path: '/network',
          name: 'network-tab',
          component: require('@/components/settings/network/NetworkTab').default
        }
      ]
    },
    {
      path: '/support',
      name: 'support-dashboard',
      component: require('@/components/support/SupportDashboard').default
    },
    {
      path: '*',
      redirect: '/'
    }
  ]
})
