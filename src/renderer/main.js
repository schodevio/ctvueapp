import Vue from 'vue'
import axios from 'axios'

import App from './App'
import router from './router'
import store from './store'

if (!process.env.IS_WEB) Vue.use(require('vue-electron'))

Vue.http = Vue.prototype.$http = axios
Vue.http.defaults.baseURL = 'http://localhost:8080/api'

Vue.config.productionTip = false

import Vuetify from 'vuetify'

Vue.use(Vuetify)

import('./../../node_modules/vuetify/dist/vuetify.min.css')

/* eslint-disable no-new */
new Vue({
  components: { App },
  router,
  store,
  template: '<App/>'
}).$mount('#app')
